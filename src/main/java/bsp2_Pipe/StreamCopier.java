package bsp2_Pipe;


import java.io.*;

public class StreamCopier {

    public static void copyByte(InputStream source, OutputStream output) throws IOException {
        int data;
        while ((data = source.read()) != -1)
            output.write(data);
    }

    public static void copyBuffered(InputStream source, OutputStream output, int size) throws IOException {
        int bytesRead;
        byte[] buffer = new byte[size];
        while ((bytesRead = source.read(buffer)) != -1)
            output.write(buffer, 0, bytesRead);
    }

    public static void main(String[] args) throws IOException {
        try{
            PipedInputStream inputStream = new PipedInputStream();
            PipedOutputStream outputStream = new PipedOutputStream(inputStream);
            FileOutputStream fileOutputStream = new FileOutputStream("src/main/resources/stream.txt");
            StreamCopier.copyByte(System.in, outputStream);
            StreamCopier.copyBuffered(inputStream, fileOutputStream, 4);
        } catch (IOException e) {
            throw new IOException(e);
        }

    }
}


