package bsp1_DeleteLines;

import java.io.*;
import java.util.*;

public class DeleteLines {
    public static Map<Integer, String> deleteline(String[] args) throws IOException {
        TreeMap<Integer, String> map = new TreeMap<>();
        File inputFile = new File(args[0]);
        File outputFile = new File(args[1]);
        try(AsciiInputStream stream = new AsciiInputStream(inputFile); FileOutputStream fos = new FileOutputStream(outputFile)){
            for (int j = 0; j < countlines(args) ; j++) {
                map.put(j, stream.readline());
            }
            for (String s: Arrays.stream(args).toList().subList(2, args.length)) {
                if(s.contains("-")){
                    Integer von = Integer.valueOf(s.split("-")[0]);
                    Integer bis = Integer.valueOf(s.split("-")[1]);
                    Iterator<Map.Entry<Integer, String>> iterator = map.entrySet().iterator();
                    while (iterator.hasNext()){
                        Map.Entry<Integer, String> z = iterator.next();
                        if(z.getKey() >= von && z.getKey()<= bis){
                            iterator.remove();
                        }
                    }
                }
                else if(!s.contains("-") && s.length()>0){
                    Iterator<Map.Entry<Integer,String>> iterator = map.entrySet().iterator();
                    while (iterator.hasNext()){
                        Map.Entry<Integer, String> z = iterator.next();
                        if(z.getKey() == Integer.parseInt(s)){
                            iterator.remove();
                        }
                    }
                }
            }
            for (Integer x:map.keySet()) {
                fos.write(map.get(x).getBytes());
            }
            return map;
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

    public static int countlines(String[] args) throws IOException {
        File inputeFile = new File(args[0]);
        try(AsciiInputStream stream = new AsciiInputStream(inputeFile)){
            byte[] b = stream.readAllBytes();
            int x = 0;
            for (byte i:b) {
                if(i == 10){
                    x++;
                }
            }
            return x;
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException();
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

    static class AsciiInputStream extends FileInputStream {


        public AsciiInputStream(File file) throws FileNotFoundException {
            super(file);
        }

        public String readline() throws IOException {
           StringBuilder sb = new StringBuilder();
            int i;
            while ((i= read())==-1){
                if(i=='\n'){
                    break;
                }else {
                    sb.append((char)i);
                }
            }

            return sb.toString();
        }
    }

    public static void main(String[] args) throws IOException {
        System.out.println(DeleteLines.deleteline(args));
    }
}
